package com.test.mvvmapp.ui.auth

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.test.mvvmapp.R
import com.test.mvvmapp.databinding.ActivityLoginBinding
import com.test.mvvmapp.ui.util.mostrar
import com.test.mvvmapp.ui.util.ocultar
import com.test.mvvmapp.ui.util.toast
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), AuthListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //porque se importa ActivityLoginBinding?? de donde sale
        //es autogenerado - el nombre va en funcion al XML relacionado a esta clase, activity_login.xml => ActivityLoginBinding
        val elBinding: ActivityLoginBinding = DataBindingUtil
            .setContentView(this, R.layout.activity_login)

        val elViewModel = ViewModelProviders
            .of(this)
            .get(AuthViewModel::class.java)

        elBinding.viewmodelLogin = elViewModel
        elViewModel.authListener = this
    }

    override fun onStarted() {
        pb_login.mostrar()
    }

    override fun onSuccess(loginResponse: LiveData<String>) {
        loginResponse.observe(this, Observer {
            pb_login.ocultar()
            toast(it)
        })
    }

    override fun onFailure(message: String) {
        pb_login.ocultar()
        toast(message)
    }

}
