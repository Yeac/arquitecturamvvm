package com.test.mvvmapp.ui.auth

import android.view.View
import androidx.lifecycle.ViewModel
import com.test.mvvmapp.data.repositories.UserRepository

class AuthViewModel : ViewModel() {

    var email: String? = null
    var password: String? = null

    //la interfaz debe ser var
    var authListener: AuthListener? = null

    fun onLoginButtonClick(view: View) {
        authListener?.onStarted()
        if (email.isNullOrEmpty() || password.isNullOrEmpty()) {
            //fail
            authListener?.onFailure("Completar email o contraseña")
            return
        }
        //exito

        //el operador !! asegura q no sea nulo
        val loginResponse = UserRepository().userLogin(email!!, password!!)
        authListener?.onSuccess(loginResponse)

    }

}